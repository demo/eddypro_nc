| branch  | status  |
| --- | --- |
| Master | [![build status](https://git.icare.univ-lille1.fr/demo/eddypro_nc/badges/master/build.svg)](https://git.icare.univ-lille1.fr/demo/eddypro_nc/commits/master) [![coverage report](https://git.icare.univ-lille1.fr/demo/eddypro_nc/badges/master/coverage.svg)](https://git.icare.univ-lille1.fr/demo/eddypro_nc/commits/master) |
| Development | [![build status](https://git.icare.univ-lille1.fr/demo/eddypro_nc/badges/develop/build.svg)](https://git.icare.univ-lille1.fr/demo/eddypro_nc/commits/develop) [![coverage report](https://git.icare.univ-lille1.fr/demo/eddypro_nc/badges/develop/coverage.svg)](https://git.icare.univ-lille1.fr/demo/eddypro_nc/commits/develop) |


# Code pour convertir les sorties text eddypro au format netCDF ACTRIS

author: Marc-Antoine Drouin
copyright: CNRS/Ecole Polytechnique 2016

## Pré-requis

Ce code fonctionne uniquement avec une version de python >=3.5

modules nécessaires pour exécuter le code:
numpy >= 1.13.0
netCDF4 >= 1.2.9
matplotlib >= 2.0.0

pour exécuter les tests:
pytest

## tester le code

se placer à la racine de ce dossier et exécuter la commande:

```bash
py.test
```

## utilisation

python eddypro2nc yyyymmdd conf/fichier_conf.py fichier_entree.csv fichier_sortie.nc
