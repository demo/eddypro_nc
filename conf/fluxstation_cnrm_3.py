# -*- coding: utf8 -*-
"""
Configuration file to create netCDF files based on eddypro output.

- instruments: LICOR LI-7500 et GILL windmaster PRO
"""

from collections import OrderedDict


# variables specific to the location of the instrument
STATION_NAME = "Meteopole-Flux"
STATION_LAT = 43.574167
STATION_LON = 1.373889
STATION_ALT = 153
SENSOR_LAT = 43.574167
SENSOR_LON = 1.373889
SENSOR_ALT = 156.5
SENSOR_ALT_AGL = 3.5
# type of instrumentation
# - 0: sonic + hygrometer
# - 1: sonic
# - 2: hygrometer
INSTR_TYPE = 0

# global attributes of the NetCDF file
GLOBAL = OrderedDict(
    [
        ("title", "heatflux at 3.5m above ground level processed by eddypro"),
        (
            "institution",
            "Météo-France, CNRM, Centre National de Recherches Météorologiques, 42 avenue G. Coriolis, 31057 Toulouse cedex",
        ),
        ("location", "Météopole"),
        (
            "source",
            "EddyPro 6.2.0 with data from GILL windmaster PRO and LICOR LI-7500",
        ),
        ("Conventions", "CF-1.6"),
        ("affiliation", "ACTRIS-FR"),
        ("production_center", "AERIS/ESPRI Data and Services Center"),
        ("acknowledgment", "to be defined"),
        (
            "contact",
            "Centre National de Recherches Météorologiques, 42 avenue G. Coriolis, 31057 Toulouse cedex",
        ),
    ]
)
