# -*- coding: utf8 -*-
"""
Configuration file to create netCDF files based on eddypro output.

- instruments: LICOR LI-7500 et Campbell Scientific CSAT3 at 30m.
"""

from collections import OrderedDict


# variables specific to the location of the instrument
STATION_NAME = "P2OA-CRA"
STATION_LAT = 43.12421
STATION_LON = 0.36260
STATION_ALT = 592
SENSOR_LAT = 43.124208
SENSOR_LON = 0.362592
SENSOR_ALT = 632.3
SENSOR_ALT_AGL = 29.3
# type of instrumentation
# - 0: sonic + hygrometer
# - 1: sonic
# - 2: hygrometer
INSTR_TYPE = 0

# global attributes of the NetCDF file
# global attributes of the NetCDF file
GLOBAL = OrderedDict(
    [
        ("title", "heatflux at 30m above ground level processed by eddypro"),
        (
            "institution",
            "Observatoire Midi-Pyrénées, P2OA-CRA, Centre de Recherches Atmosphériques, 8 route de Lannemezan, 65300 Campistrous",
        ),
        ("location", "P2OA-CRA (43.124208N, 0.362592E)"),
        (
            "source",
            "EddyPro 6.2.0 with data from Campbell Scientific CSAT3 and LICOR LI-7500",
        ),
        ("Conventions", "CF-1.6"),
        ("affiliation", "ACTRIS-FR"),
        ("production_center", "AERIS/ESPRI Data and Services Center"),
        ("acknowledgment", "to be defined"),
        (
            "contact",
            "P2OA-CRA, Centre de Recherches Atmospheriques, 8 route de Lannemezan, 65300 CAMPISTROUS",
        ),
    ]
)
