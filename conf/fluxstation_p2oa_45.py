# -*- coding: utf8 -*-
"""
Configuration file to create netCDF files based on eddypro output.

- instruments: GILL windmaster PRO at 45m.
"""

from collections import OrderedDict


# variables specific to the location of the instrument
STATION_NAME = "P2OA-CRA"
STATION_LAT = 43.12421
STATION_LON = 0.36260
STATION_ALT = 592
SENSOR_LAT = 43.124208
SENSOR_LON = 0.362592
SENSOR_ALT = 648.8
SENSOR_ALT_AGL = 45.8
# type of instrumentation
# - 0: sonic + hygrometer
# - 1: sonic
# - 2: hygrometer
INSTR_TYPE = 1

# global attributes of the NetCDF file
GLOBAL = OrderedDict(
    [
        ("title", "heatflux at 45m above ground level processed by eddypro"),
        (
            "institution",
            "Observatoire Midi-Pyrénées, P2OA-CRA, Centre de Recherches Atmosphériques, 8 route de Lannemezan, 65300 Campistrous",
        ),
        ("location", "P2OA-CRA (43.124208N, 0.362592E)"),
        ("source", "EddyPro 6.2.0 with data from Gill windmaster PRO"),
        ("Conventions", "CF-1.6"),
        ("affiliation", "ACTRIS-FR"),
        ("production_center", "AERIS/ESPRI Data and Services Center"),
        ("acknowledgment", "to be defined"),
        (
            "contact",
            "P2OA-CRA, Centre de Recherches Atmospheriques, 8 route de Lannemezan, 65300 CAMPISTROUS",
        ),
    ]
)
