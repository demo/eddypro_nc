# -*- coding: utf8 -*-
"""
Configuration file to create netCDF files based on eddypro output.

- instruments: LICOR LI-7200 et Gill windmaster Pro at 30m.
"""

from collections import OrderedDict


# variables specific to the location of the instrument
STATION_NAME = "SIRTA"
STATION_LAT = 48.713
STATION_LON = 2.208
STATION_ALT = 156
SENSOR_LAT = 48.717687
SENSOR_LON = 2.207603
SENSOR_ALT = 186
SENSOR_ALT_AGL = 30
# type of instrumentation
# - 0: sonic + hygrometer
# - 1: sonic
# - 2: hygrometer
INSTR_TYPE = 0

# global attributes of the NetCDF file
GLOBAL = OrderedDict(
    [
        ("title", "heatflux at 30m above ground level processed by eddypro"),
        ("institution", "IPSL (CNRS/Ecole Polytechnique)"),
        ("location", "SIRTA "),
        (
            "source",
            "EddyPro 6.2.0 with data from Gill windmaster PRO and LICOR LI-7200",
        ),
        ("Conventions", "CF-1.6"),
        ("affiliation", "ACTRIS-FR"),
        ("production_center", "AERIS/ESPRI Data and Services Center"),
        ("acknowledgment", "to be defined"),
        ("contact", "sirtascience@ipsl.polytechnique.fr"),
    ]
)
