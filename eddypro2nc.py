#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Code to eddypro fulloutput file into netCDF file

copyright: CNRS/Ecole Polytechnique 2016
"""

from __future__ import print_function, division, absolute_import

import sys
import argparse
from itertools import chain
import datetime as dt

import numpy as np
import pandas as pd
import netCDF4 as nc

from utils import utils
from utils import common


__author__ = "marc-antoine Drouin"
__version__ = "0.1.12"


CONF = {
    "skip_header": 3,
    "missing_values": ["-9999.0"],
    "delimiter": ",",
    "date_parser": lambda str_: dt.datetime.strptime(str_, "%Y-%m-%d %H:%M"),
    "index_col": "date_time",
    "parse_dates": [[1, 2]],
}

PANDAS_DATE_FMT = "{:%Y-%m-%d}"

NC_FORMAT = "NETCDF3_CLASSIC"
MISSING_INT = -9
MISSING_FLOAT = np.nan
TIME_UNITS = "minutes since 1970-01-01 00:00:00 UTC"
NC_HISTORY_FMT = "%Y-%m-%dT%H:%M:%SZ"
TIME_CALENDAR = "standard"


def get_columns_names(file_):
    """Extract columns names from file second line."""
    print("extract columns name")

    with open(file_, "r", encoding='utf8') as f_id:
        f_id.readline()
        header = f_id.readline().strip()

    col_names = header.split(CONF["delimiter"])

    return col_names


def get_time_resolution(data):
    """Determine time resolution of data."""
    # difference between timestep
    diff = data.index.values[1::] - data.index.values[0:-1]
    # convert to minutes then to int
    diff = diff.astype("timedelta64[m]") / np.timedelta64(1, "m")

    # get unique value
    time_resol = np.unique(diff)

    if time_resol.size != 1:
        print("problem with data time resolution. Quitting.")
        sys.exit(1)

    return time_resol[0]


def read_data(file_):
    """Read the data from eddypro fulloutput file and store it as numpy.ndarray."""
    col_names = get_columns_names(file_)

    print("reading {}".format(file_))

    data = pd.read_csv(
        file_,
        skiprows=CONF["skip_header"],
        delimiter=CONF["delimiter"],
        na_values=CONF["missing_values"],
        names=col_names,
        parse_dates=CONF["parse_dates"],
        date_parser=CONF["date_parser"],
        index_col=CONF["index_col"],
    )

    # if no H2O measurement air_temp = virt_temp
    # we check it and remove air_temp data if there is no H2O
    if np.all(np.isnan(data.h2o_mixing_ratio.values)):
        data.loc[:, "air_temperature"] = np.nan

    # timestamp are at the end of mean period.
    # We change it to the start to ease time filtering of data
    time_res = get_time_resolution(data)
    data.index = data.index - pd.Timedelta(minutes=time_res)

    return data


def read_multifiles(list_files):
    """Read several files and concatenate data in one dataframe."""
    tmp_list = []
    for file_ in list_files:

        tmp = read_data(file_)
        tmp_list.append(tmp)
        print(file_)

    data = pd.concat(tmp_list)

    return data


def create_netcdf(file_, data, conf):
    """Save the data into a netCDF file."""
    nc_id = nc.Dataset(file_, "w", data_model=conf["NC_FORMAT"])

    # global attributes
    # ------------------------------------------------------------------------
    g_atts = conf["GLOBAL"]
    for att in g_atts:
        setattr(nc_id, att, g_atts[att])

    # add history
    history_msg = "created the {} with eddypro2nc v{}"
    setattr(
        nc_id,
        "history",
        history_msg.format(dt.datetime.now().strftime(NC_HISTORY_FMT), __version__),
    )

    # dimensions
    # ------------------------------------------------------------------------
    nc_id.createDimension("time", size=None)
    nc_id.createDimension("nv", size=2)
    nc_id.createDimension("strlen", size=len(conf["STATION_NAME"]))

    # variables
    # ------------------------------------------------------------------------

    # time
    var = nc_id.createVariable("time", "f8", ("time",))
    var.standard_name = "time"
    var.units = conf["TIME_UNITS"]
    var.calendar = conf["TIME_CALENDAR"]
    var.axis = "T"
    var.bounds = "time_bnds"
    var[:] = nc.date2num(data["datetime"], units=conf["TIME_UNITS"])

    # time bounds
    time_bnds = np.ones((data["datetime"].size, 2), dtype=np.dtype(dt.datetime))
    time_bnds[:, 0] = data["datetime"]
    time_bnds[:, 1] = data["datetime"] + dt.timedelta(minutes=conf["TIME_RES_MINUTE"])

    var = nc_id.createVariable("time_bnds", "f8", ("time", "nv"))
    var[:] = nc.date2num(time_bnds, units=conf["TIME_UNITS"])

    # station localization
    var = nc_id.createVariable("station_name", "S1", ("strlen",))
    var.long_name = "station name"
    var.cf_role = "timeseries_id"
    var[:] = nc.stringtoarr(conf["STATION_NAME"], len(conf["STATION_NAME"]))

    var = nc_id.createVariable("station_lat", "f4")
    var.standard_name = "latitude"
    var.units = "degree_north"
    var[:] = conf["STATION_LAT"]

    var = nc_id.createVariable("station_lon", "f4")
    var.standard_name = "longitude"
    var.units = "degree_east"
    var[:] = conf["STATION_LON"]

    var = nc_id.createVariable("station_alt", "f4")
    var.standard_name = "altitude"
    var.long_name = "height above mean sea level"
    var.positive = "up"
    var.units = "m"
    var[:] = conf["STATION_ALT"]

    # sensor localization
    var = nc_id.createVariable("sensor_lat", "f4")
    var.standard_name = "latitude"
    var.units = "degree_north"
    var[:] = conf["SENSOR_LAT"]

    var = nc_id.createVariable("sensor_lon", "f4")
    var.standard_name = "longitude"
    var.units = "degree_east"
    var[:] = conf["SENSOR_LON"]

    var = nc_id.createVariable("sensor_alt", "f4")
    var.standard_name = "altitude"
    var.long_name = "height above mean sea level"
    var.positive = "up"
    var.units = "m"
    var[:] = conf["SENSOR_ALT"]

    var = nc_id.createVariable("sensor_alt_agl", "f4")
    var.standard_name = "altitude"
    var.long_name = "vertical distance above the surface"
    var.positive = "up"
    var.units = "m"
    var[:] = conf["SENSOR_ALT_AGL"]

    # type installation
    var = nc_id.createVariable("instrument_type", "i2")
    var.long_name = "type of instruments"
    var.flag_values = np.array([0, 1, 2], dtype="i2")
    var.flag_meanings = "sonic_anemometer_and_hygrometer sonic_anemometer hygrometer"
    var[:] = conf["INSTR_TYPE"]

    # variables in the file
    for var in common.VARIABLES:

        print("processing {}".format(var))

        data_key = common.VARIABLES[var]["raw_name"]
        data_type = common.VARIABLES[var]["type"]
        fill_value = common.VARIABLES[var]["fillvalue"]
        attributes = common.VARIABLES[var]["attributes"]
        factor = common.VARIABLES[var]["preprocess"]["factor"]
        offset = common.VARIABLES[var]["preprocess"]["offset"]

        if factor is not None:
            data[data_key] = data[data_key] * factor

        if offset is not None:
            data[data_key] = data[data_key] + offset

        if data_type in ["i2", "i4", "i8"]:
            print("changing type of {} ({})".format(var, data_key))
            data[data_key][np.isnan(data[data_key])] = conf["MISSING_INT"]
            data[data_key] = data[data_key].astype(int)

        var = nc_id.createVariable(var, data_type, ("time"), fill_value=fill_value)
        var[:] = data[data_key]
        for att in attributes:
            setattr(var, att, attributes[att])
        # add original name of variable
        setattr(var, "EP_name", data_key)

    nc_id.close()


def eddypro_to_nc(argv):
    """Convert data to netCDF."""
    # Process arguments
    # ------------------------------------------------------------------------
    parser = utils.init_arg_parser()
    try:
        input_args = parser.parse_args(argv)
    except argparse.ArgumentError as exc:
        print("\n", exc)
        sys.exit(1)

    # extract arguments
    date = input_args.date
    # convert module into dict
    conf = {
        k: getattr(input_args.conf, k) for k in dir(input_args.conf) if "__" not in k
    }
    # file_meta = utils.read_metadata(input_args.metadata)
    file_in = [f for f in chain.from_iterable(input_args.input_file)]
    file_ou = input_args.output_file

    print("files to read")
    for i_file, file_ in enumerate(file_in):
        print("{:02d}: {}".format(i_file, file_))

    print("output file name: {}".format(file_ou))

    # Read data
    # ------------------------------------------------------------------------
    data = read_multifiles(file_in)

    # Filter data to only keep the wanted day
    # ------------------------------------------------------------------------
    data = data[PANDAS_DATE_FMT.format(date)]

    # complete conf
    # ------------------------------------------------------------------------
    conf["TIME_RES_MINUTE"] = get_time_resolution(data)
    conf["NC_FORMAT"] = NC_FORMAT
    conf["MISSING_INT"] = MISSING_INT
    conf["MISSING_FLOAT"] = MISSING_FLOAT
    conf["TIME_UNITS"] = TIME_UNITS
    conf["TIME_CALENDAR"] = TIME_CALENDAR

    # store data as dict to simplify use for netCDF creation
    # ------------------------------------------------------------------------
    data = utils.df_to_dict(data)

    # create NetCDF file
    # ------------------------------------------------------------------------
    create_netcdf(file_ou, data, conf)

    # End
    # ------------------------------------------------------------------------
    sys.exit(0)


if __name__ == "__main__":
    eddypro_to_nc(sys.argv[1:])
