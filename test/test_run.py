#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

import subprocess
import os

import pytest


MAIN_DIR = os.path.dirname(os.path.dirname(__file__))
CONF_DIR = os.path.join(MAIN_DIR, 'conf')
TEST_DIR = os.path.join(MAIN_DIR, 'test')
TEST_IN_DIR = os.path.join(TEST_DIR, 'input')
TEST_OUT_DIR = os.path.join(TEST_DIR, 'output')
PRGM = "eddypro2nc.py"

@pytest.mark.parametrize('date, file_in, file_conf', [
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-10min-Mean-30m-Alt_2017-08-14T00-00-00_1D_V1-00.csv',
        'fluxstation_p2oa_30.py'
    ),
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-10min-Mean-45m-Alt_2017-08-14T00-00-00_1D_V1-00.csv',
        'fluxstation_p2oa_45.py'
    ),
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-10min-Mean-60m-Alt_2017-08-14T00-00-00_1D_V1-00.csv',
        'fluxstation_p2oa_60.py'
    ),
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-30min-Mean-30m-Alt_2017-08-14T00-00-00_1D_V1-00.csv',
        'fluxstation_p2oa_30.py'
    ),
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-30min-Mean-45m-Alt_2017-08-14T00-00-00_1D_V1-00.csv',
        'fluxstation_p2oa_45.py'
    ),
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-30min-Mean-60m-Alt_2017-08-14T00-00-00_1D_V1-00.csv',
        'fluxstation_p2oa_60.py'
    ),
    (
        '20171018',
        'SIRTA_EP-Eddy-Covariance_L2A-10min-Mean-2m-Alt_2017-10-18T00-00-00_1D_V1-00.csv',
        'fluxstation_sirta_2.py',
    ),
    (
        '20171018',
        'SIRTA_EP-Eddy-Covariance_L2A-10min-Mean-30m-Alt_2017-10-18T00-00-00_1D_V1-00.csv',
        'fluxstation_sirta_30.py',
    ),
    (
        '20171018',
        'SIRTA_EP-Eddy-Covariance_L2A-30min-Mean-2m-Alt_2017-10-18T00-00-00_1D_V1-00.csv',
        'fluxstation_sirta_2.py',
    ),
    (
        '20171018',
        'SIRTA_EP-Eddy-Covariance_L2A-30min-Mean-30m-Alt_2017-10-18T00-00-00_1D_V1-00.csv',
        'fluxstation_sirta_30.py',
    ),
    (
        '20171215',
        'CNRM_EP-Eddy-Covariance_L2A-30min-Mean-3.5m-Alt_2017-12-15T00-00-00_1D_V1-00.csv',
        'fluxstation_cnrm_3.py',
    )
])
def test_run(date, file_in, file_conf):
    """Test production of netCDF file."""
    files_in = os.path.join(TEST_IN_DIR, file_in)
    file_out = os.path.join(TEST_OUT_DIR, file_in.replace('.csv', '.nc'))
    file_conf = os.path.join(CONF_DIR, file_conf)

    resp = subprocess.call([
        os.path.join(MAIN_DIR, PRGM),
        date,
        file_conf,
        files_in,
        file_out,
    ])

    assert resp == 0
