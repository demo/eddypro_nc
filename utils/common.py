#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from collections import OrderedDict

import numpy as np

# variables of the NetCDF file
VARIABLES = OrderedDict(
    [
        (
            "u",
            {
                "raw_name": "u_unrot",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "u wind speed",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m.s^-1",
                    "comment": "wind component along the u anemometer axis",
                },
            },
        ),
        (
            "v",
            {
                "raw_name": "v_unrot",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "v wind spped",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m.s^-1",
                    "comment": "wind component along the v anemometer axis",
                },
            },
        ),
        (
            "w",
            {
                "raw_name": "w_unrot",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "w wind speed",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m.s^-1",
                    "comment": "wind component along the w anemometer axis",
                },
            },
        ),
        (
            "ws",
            {
                "raw_name": "wind_speed",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "wind_speed",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m.s^-1",
                    "ancillary_variables": "ws_max",
                },
            },
        ),
        (
            "wd",
            {
                "raw_name": "wind_dir",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "wind_from_direction",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "degree",
                },
            },
        ),
        (
            "ws_max",
            {
                "raw_name": "max_wind_speed",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "wind_speed_of_gust",
                    "units": "m.s^-1",
                    "cell_methods": "time: maximum",
                },
            },
        ),
        (
            "virt_temp",
            {
                "raw_name": "sonic_temperature",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "sonic temperature",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "K",
                    "comment": "measured with sonic anemometer",
                },
            },
        ),
        (
            "air_temp",
            {
                "raw_name": "air_temperature",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "air_temperature",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "K",
                },
            },
        ),
        (
            "rh",
            {
                "raw_name": "RH",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "relative_humidity",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "%",
                },
            },
        ),
        (
            "dew_temp",
            {
                "raw_name": "Tdew",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "dew_point_temperature",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "K",
                },
            },
        ),
        (
            "air_density",
            {
                "raw_name": "air_density",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "air_density",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "kg.m^-3",
                },
            },
        ),
        (
            "air_heat_capacity",
            {
                "raw_name": "air_heat_capacity",
                "preprocess": {"factor": 1.e-3, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "specific heat at constant pressure of ambient air",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "kJ.kg^-1.K^-1",
                },
            },
        ),
        (
            "e",
            {
                "raw_name": "e",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "ambient water vapor partial pressure",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "Pa",
                },
            },
        ),
        (
            "h2o_mixing_ratio",
            {
                "raw_name": "h2o_mixing_ratio",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "humidity_mixing_ratio",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "1e-3 mol.mol^-1",
                },
            },
        ),
        (
            "co2_mixing_ratio",
            {
                "raw_name": "co2_mixing_ratio",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "carbon dioxide mixing ratio",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "1e-6 mol.mol^-1",
                },
            },
        ),
        (
            "tau",
            {
                "raw_name": "Tau",
                "preprocess": {"factor": None, "offset": None},
                "type": "f8",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "corrected momentum flux",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "kg.m^-1.s^-2",
                    "ancillary_variables": "tau_qc tau_err",
                },
            },
        ),
        (
            "tau_qc",
            {
                "raw_name": "qc_Tau",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "corrected momentum flux status_flag",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "flag_values": np.array([0, 1, 2], dtype="i4"),
                    "flag_meanings": "high_quality suitable_for_budget_analysis bad_quality",
                    "comments": "Mauder and Foken, 2004",
                },
            },
        ),
        (
            "tau_err",
            {
                "raw_name": "rand_err_Tau",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "corrected momentum flux random_error",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "kg.m^-1.s^-2",
                },
            },
        ),
        (
            "H",
            {
                "raw_name": "H",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "upward_sensible_heat_flux_in_air",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "W.m^-2",
                    "comment": '"Downward" indicates a vector component which is positive when directed downward (negative upward)',
                    "ancillary_variables": "H_qc H_err",
                },
            },
        ),
        (
            "H_qc",
            {
                "raw_name": "qc_H",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "upward_sensible_heat_flux_in_air status_flag",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "flag_values": np.array([0, 1, 2], dtype="i4"),
                    "flag_meanings": "high_quality suitable_for_budget_analysis bad_quality",
                    "comment": "Mauder and Foken, 2004",
                },
            },
        ),
        (
            "H_err",
            {
                "raw_name": "rand_err_H",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "upward_sensible_heat_flux_in_air random_error",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "W.m^-2",
                },
            },
        ),
        (
            "LE",
            {
                "raw_name": "LE",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "upward_latent_heat_flux_in_air",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "W.m^-2",
                    "comment": '"Downward" indicates a vector component which is positive when directed downward (negative upward).',
                },
            },
        ),
        (
            "LE_qc",
            {
                "raw_name": "qc_LE",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "upward_latent_heat_flux_in_air status_flag",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "flag_values": np.array([0, 1, 2], dtype="i4"),
                    "flag_meanings": "high_quality suitable_for_budget_analysis bad_quality",
                    "comment": "Mauder and Foken, 2004",
                },
            },
        ),
        (
            "LE_err",
            {
                "raw_name": "rand_err_LE",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "upward_latent_heat_flux_in_air random_error",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "W.m^-2",
                },
            },
        ),
        (
            "bowen_ratio",
            {
                "raw_name": "bowen_ratio",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "bowen ratio",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "1",
                    "comment": "is the ratio of energy fluxes from one state to another by sensible heat and latent heating respectively",
                },
            },
        ),
        (
            "h2o_flux",
            {
                "raw_name": "h2o_flux",
                "preprocess": {"factor": 18e-6, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "standard_name": "upward_water_vapor_flux_in_air",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "kg.m^-2.s^-1",
                    "comment": 'The surface called "surface" means the lower boundary of the atmosphere. "Water" means water in all phases, including frozen i.e. ice and snow. "Downward" indicates a vector component which is positive when directed downward (negative upward)',
                    "ancillary_variables": "h2o_flux_qc h2o_flux_err",
                },
            },
        ),
        (
            "h2o_flux_qc",
            {
                "raw_name": "qc_h2o_flux",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "upward_water_vapor_flux_in_air status_flag",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "flag_values": np.array([0, 1, 2], dtype="i4"),
                    "flag_meanings": "high_quality suitable_for_budget_analysis bad_quality",
                    "comment": "Mauder and Foken, 2004",
                },
            },
        ),
        (
            "h2o_flux_err",
            {
                "raw_name": "rand_err_h2o_flux",
                "preprocess": {"factor": 18e-6, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "upward_water_vapor_flux_in_air random_error",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "kg.m^-2.s^-1",
                },
            },
        ),
        (
            "co2_flux",
            {
                "raw_name": "co2_flux",
                "preprocess": {"factor": 1e-6, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "upward carbon dioxide flux in air",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "mol.m^-2.s^-1",
                    "comment": 'The surface called "surface" means the lower boundary of the atmosphere. "Water" means water in all phases, including frozen i.e. ice and snow. "Downward" indicates a vector component which is positive when directed downward (negative upward)',
                    "ancillary_variables": "h2o_flux_qc h2o_flux_err",
                },
            },
        ),
        (
            "co2_flux_qc",
            {
                "raw_name": "qc_co2_flux",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "upward carbon dioxide flux in air status_flag",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "flag_values": np.array([0, 1, 2], dtype="i4"),
                    "flag_meanings": "high_quality suitable_for_budget_analysis bad_quality",
                    "comment": "Mauder and Foken, 2004",
                },
            },
        ),
        (
            "co2_flux_err",
            {
                "raw_name": "rand_err_co2_flux",
                "preprocess": {"factor": 1e-6, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "upward carbon dioxide flux in air random_error",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "mol.m^-2.s^-1",
                },
            },
        ),
        (
            "h2o_molar_density",
            {
                "raw_name": "h2o_molar_density",
                "preprocess": {"factor": 1e-3, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "h2o molar density",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "mol.m^-3",
                },
            },
        ),
        (
            "co2_molar_density",
            {
                "raw_name": "co2_molar_density",
                "preprocess": {"factor": 1e-6, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "co2 molar density",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "mol.m^-3",
                },
            },
        ),
        (
            "u_star",
            {
                "raw_name": "u*",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "friction velocity",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m.s^-1",
                    "comment": "It is usually applied to motion near the ground where the shearing stress is often assumed to be independent of height and approximately proportional to the square of the mean velocity",
                },
            },
        ),
        (
            "TKE",
            {
                "raw_name": "TKE",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "turbulent kinetic energy",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m^2.s^-2",
                },
            },
        ),
        (
            "L",
            {
                "raw_name": "L",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "Monin-Obukhov Length",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m",
                    "comment": "The Obukhov length is used to describe the effects of buoyancy on turbulent flows, particularly in the lower tenth of the atmospheric boundary layer",
                },
            },
        ),
        (
            "z_minus_d_over_L",
            {
                "raw_name": "(z-d)/L",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "Monin-Obukhov stability parameter",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "1",
                },
            },
        ),
        (
            "u_var",
            {
                "raw_name": "u_var",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "along wind component variance",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m^2.s^-2",
                    "cell_methods": "time: variance",
                },
            },
        ),
        (
            "v_var",
            {
                "raw_name": "v_var",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "cross wind component variance",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m^2.s^-2",
                    "cell_methods": "time: variance",
                },
            },
        ),
        (
            "w_var",
            {
                "raw_name": "w_var",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "vertical_wind variance",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m^2.s^-2",
                    "cell_methods": "time: variance",
                },
            },
        ),
        (
            "sonic_air_temp_var",
            {
                "raw_name": "ts_var",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "variance of virtual temperature of air measured with sonic anemometer",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "K^2",
                    "cell_methods": "time: variance",
                },
            },
        ),
        (
            "u_spikes",
            {
                "raw_name": "u_spikes",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "u spikes number",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "count",
                    "comment": "number of spikes detected for u variable in each time step",
                },
            },
        ),
        (
            "v_spikes",
            {
                "raw_name": "v_spikes",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "v spikes number",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "count",
                    "comment": "number of spikes detected for v variable in each time step",
                },
            },
        ),
        (
            "w_spikes",
            {
                "raw_name": "w_spikes",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "w spikes number",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "count",
                    "comment": "number of spikes detected for w variable in each time step",
                },
            },
        ),
        (
            "ts_spikes",
            {
                "raw_name": "ts_spikes",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "ts spikes number",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "count",
                    "comment": "Number of spikes detected for virt_temp variable in each time step",
                },
            },
        ),
        (
            "h2o_spikes",
            {
                "raw_name": "h2o_spikes",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "h2o spikes number",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "count",
                    "comment": "number of spikes detected for h2o variable in each time step",
                },
            },
        ),
        (
            "co2_spikes",
            {
                "raw_name": "co2_spikes",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "co2 spikes number",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "count",
                    "comment": "Number of spikes detected for co2 variable in each time step",
                },
            },
        ),
        (
            "w_ts_cov",
            {
                "raw_name": "w/ts_cov",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "covariance between vertical wind and virtual temperature of the air",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "m.s^-1.K",
                },
            },
        ),
        (
            "w_h2o_cov",
            {
                "raw_name": "w/h2o_cov",
                "preprocess": {"factor": 18e-6, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "covariance between vertical wind and water vapor",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "kg.s^-2.m^-1",
                },
            },
        ),
        (
            "w_co2_cov",
            {
                "raw_name": "w/co2_cov",
                "preprocess": {"factor": 1e-6, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "covariance between vertical wind and carbon dioxide",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "mol.s^-2.m^-1",
                },
            },
        ),
        (
            "footprint",
            {
                "raw_name": "model",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "model for footprint estimation",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "1",
                    "flag_values": np.array([0, 1], dtype="i4"),
                    "flag_meanings": "kljun_et_al_2004 kormann_and_meixner_2001",
                    "comment": "The flux footprint functions estimate the location and relative importance of passive scalar sources influencing flux measurements at a given receptor height, depending on receptor height, atmospheric stability, and surface roughness (Kljun et al., 2004). Regardless of the chosen model, the footprint estimation is provided as a set of distances from the anemometer location in the direction from which the wind blows.",
                },
            },
        ),
        (
            "used_record",
            {
                "raw_name": "used_records",
                "preprocess": {"factor": None, "offset": None},
                "type": "i4",
                "fillvalue": -9,
                "attributes": {
                    "long_name": "used record",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "count",
                    "comment": "number of valid records used for the current averaging period",
                },
            },
        ),
        (
            "h2o_timelag",
            {
                "raw_name": "h2o_time_lag",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "h2o timelag",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "s",
                    "comment": "Time lag used to synchronize h2o time series",
                },
            },
        ),
        (
            "co2_timelag",
            {
                "raw_name": "co2_time_lag",
                "preprocess": {"factor": None, "offset": None},
                "type": "f4",
                "fillvalue": np.nan,
                "attributes": {
                    "long_name": "co2 timelag",
                    "coordinates": "time sensor_lat sensor_lon sensor_alt station_name",
                    "units": "s",
                    "comment": "Time lag used to synchronize co2 time series",
                },
            },
        ),
        # ('', {
        #     'raw_name': '',
        #     'preprocess': {
        #         'factor': None,
        #         'offset': None,
        #     },
        #     'type': 'f4',
        #     'fillvalue': np.nan,
        #     'attributes': {
        #         'long_name': '',
        #         'coordinates': 'time sensor_lat sensor_lon sensor_alt station_name',
        #         'units': '',
        #         'comment': '',
        #     }
        # }),
    ]
)
