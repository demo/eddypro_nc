#!/usr/bin/env python
# -*- code: utf8 -*-

import sys
import os
import glob
import argparse
import importlib
import configparser
import datetime as dt

import numpy as np


DATE_FMT = "%Y%m%d"

# program description
PROG_DESC = """
    convert eddypro fulloutput files into netCDF file
    """


def check_date_format(input_date):
    """
    Check the format of the date argument
    """

    try:
        dt_date = dt.datetime.strptime(input_date, DATE_FMT)
    except:
        msg = "%r has not the required format (YYYYMMDD)" % input_date
        raise argparse.ArgumentTypeError(msg)

    return dt_date


def check_dir(dir_name):
    """
    Check if a directory exists and is writable
    """

    return os.access(dir_name, os.W_OK)


def check_input_files(input_files):
    """
    check if the input files exist and return a list of the input files found
    """

    list_files = glob.glob(input_files)

    if len(list_files) == 0:
        msg = "No input files found corresponding to the file pattern "
        msg += input_files
        raise argparse.ArgumentTypeError(msg)

    return sorted(list_files)


def check_output_dir(output_file):
    """
    check if the directory provided for the output file is writable
    """

    output_file = os.path.abspath(output_file)
    out_dir = os.path.dirname(output_file)

    if not check_dir(out_dir):
        msg = "output directory " + out_dir
        msg += " doesn't exist or is not writable"
        raise argparse.ArgumentTypeError(msg)

    return output_file


def check_conf_file(input_conf):
    """check if conf file can be loaded and load it if possible"""

    list_files = glob.glob(input_conf)

    if len(list_files) != 1:
        msg = "error: Configuration file {} cannot be found"
        raise argparse.ArgumentError("conf", msg.format(input_conf))

    # extract path
    file_ = os.path.abspath(list_files[0])
    module_path = os.path.dirname(file_)
    module_name = os.path.splitext(os.path.basename(file_))[0]

    # check if module path is in python path. if not add it
    if module_path not in sys.path:
        sys.path.append(module_path)

    try:
        conf = importlib.import_module(module_name)
    except ImportError:
        msg = "error: cannot import module {}"
        raise argparse.ArgumentError("conf", msg.format(list_files[0]))

    return conf


def init_arg_parser():
    """Init inputs arguments parser."""
    parser = argparse.ArgumentParser(description=PROG_DESC)

    parser.add_argument(
        "date", type=check_date_format, help="date to process (yyyymmdd)"
    )
    parser.add_argument("conf", type=check_conf_file, help="configuration file (*.py)")
    parser.add_argument("input_file", type=check_input_files, nargs="*")
    parser.add_argument(
        "output_file", type=check_output_dir, help="Name of the output file"
    )
    parser.add_argument(
        "-m",
        "--mode",
        choices=["i", "p"],
        default="p",
        help="interactive or prod (change what is printed)",
    )

    return parser


def read_metadata(file_id):
    """Read eddypro metadata file and return only needed data."""
    # load metadata file
    raw_metadata = configparser.ConfigParser()
    raw_metadata.read_file(file_id)

    # extract needed metadata
    metadata = {}
    metadata["eddypro_version"] = raw_metadata["Project"]["sw_version"]
    metadata["sensor_lat"] = float(raw_metadata["Site"]["latitude"])
    metadata["sensor_lon"] = float(raw_metadata["Site"]["longitude"])
    metadata["sensor_alt"] = float(raw_metadata["Site"]["altitude"])
    metadata["station_name"] = raw_metadata["Station"]["station_name"]

    return metadata


def df_to_dict(data_df):
    """Convert selected columns of dataframe into a dictionary."""
    out = {}

    for col in data_df.columns.values:
        print(col)
        out[col] = data_df[col].values

    # convert index into datetime
    out["datetime"] = data_df.index.to_pydatetime()

    return out


def print_progress(iteration, total, prefix="", suffix="", decimals=2, bar_length=100):
    """
    Call in a loop to create terminal progress bar.

    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : number of decimals in percent complete (Int)
        barLength   - Optional  : character length of bar (Int)
    """
    filled_length = int(round(bar_length * iteration / float(total)))
    percents = round(100.00 * (iteration / float(total)), decimals)
    progress_bar = "*" * filled_length + "-" * (bar_length - filled_length)
    sys.stdout.write(
        "\r%s |%s| %s%s %s" % (prefix, progress_bar, percents, "%", suffix)
    )
    sys.stdout.flush()
    if iteration == total:
        sys.stdout.write("\n")
        sys.stdout.flush()
